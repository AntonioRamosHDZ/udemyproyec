const { response } = require('express')
const Usuario = require('../models/usuario')
const bcryptjs = require('bcryptjs');
const { generarJWT } = require('../helpers/generar-jwt');



const login = async (req,res = response) =>{

    const{correo,password}= req.body;
    
    try {
        //Verificar si el email existe
       const usuario = await Usuario.findOne({correo});
       if (!usuario){
        return res.status(400).json({
            msg: 'Usuario / Password no son correctos - correo'
        });
       }


        // Si el usuario esta activo
        if (!usuario.estado){
            return res.status(400).json({
                msg: 'Usuario / Password no son correctos - estado: false'
            });
           }

        //Verificar la contraseña
        const salto = bcryptjs.genSaltSync(10);
        const contra = bcryptjs.hashSync(usuario.password, salto)
        const validPassword = bcryptjs.compareSync(password,contra)
          

           if (!validPassword){
            return res.status(400).json({
                msg:'Usuario / Password no son correctos - password',
                pass: bcryptjs.hashSync(password,salto)
            })
           }

        // Generar el JWT
           const token =await generarJWT(usuario.id)


        res.json({
            usuario,
            token
        })


    } catch (error) {
        console-console.log(error)
        res.status(500).json({
            msg: 'Hable con el administrador'
        });
        
    }

}

module.exports={
    login

}